import express from "express";
import ProductRoutes from "./product/productRoutes.js";
import ChatRoutes from "./chat/chatRoutes.js";

import { getAllProducts } from "./product/productService.js";
import { saveNewMessage } from "./chat/chatService.js"

import { Server } from "socket.io";
import { createServer } from "http";

const app = express();
const port: number = 8080;
const baseUrl: string = "/api";

const server = createServer(app);
const io = new Server(server);
app.locals.io = io;

io.on("connection", (socket) => {
  console.log("a user connected");

  const products = getAllProducts();
  io.emit("initialization", { products });

  socket.on("new message", (msg) => {
    io.emit("messages", msg);
    saveNewMessage(msg);

  });

});

app.use(express.json());
app.use(express.static("public"));

app.use(baseUrl, ProductRoutes);
app.use(baseUrl, ChatRoutes);


server.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`);
});
