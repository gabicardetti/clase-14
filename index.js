const express = require("express");

const ProductRoutes = require("./product/productRoutes.js");
const ChatRoutes = require("./chat/chatRoutes.js");

const ProductService = require("./product/productService.js");
const chatService = require("./chat/chatService.js")

const socketIo = require("socket.io");
const http = require("http");

const app = express();
const port = 8080;
const baseUrl = "/api";

const server = http.createServer(app);
const io = new socketIo.Server(server);
app.locals.io = io;

io.on("connection", (socket) => {
  console.log("a user connected");

  const products = ProductService.getAllProducts();
  io.emit("initialization", { products });

  socket.on("new message", (msg) => {
    io.emit("messages", msg);
    chatService.saveNewMessage(msg);

  });

});

app.use(express.json());
app.use(express.static("public"));

app.use(baseUrl, ProductRoutes);
app.use(baseUrl, ChatRoutes);


server.listen(port, async () => {
  console.log(`app listening at http://localhost:${port}`);
});
