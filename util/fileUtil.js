const Util = require("./utils.js");

class File {
  constructor(fileName) {
    this.filePath = "./" + fileName + ".txt";
  }

  async read() {
    let file;
    try {
      file = await Util.readFile(this.filePath);
    } catch (err) {
      return [];
    }
    return file;
  }

  async save(object) {
    let newFileContent = object;

    try {
      let oldObjects = await this.read(this.filePath);
      if (!Array.isArray(oldObjects)) oldObjects = JSON.parse(oldObjects);
      if (oldObjects.length == 0) newFileContent = [object];
      else {
        newFileContent = [...oldObjects, object];
      }
    } catch (e) {
      console.log(e);
    }

    try {
      await Util.writeFile(this.filePath, JSON.stringify(newFileContent));
    } catch (err) {
      console.log(err);
    }
  }
}

module.exports = File;
