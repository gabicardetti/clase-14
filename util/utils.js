const fs = require("fs");

function generateRandomNumberFloat(min, max) {
    return Math.random() * (max - min + 1) + min;
};

function generateRandomNumber(min, max) {
    return Math.floor(
        Math.random() * (max - min + 1) + min
    )
}

async function readFile(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', function (err, data) {
            if (err) {
                reject(err);
            }
            resolve(data);
        });
    });
}

async function writeFile(path, content) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, content, function (err, data) {
            if (err) {
                reject(err);
            }
            resolve(data);
        });
    });
}

module.exports = {
    generateRandomNumberFloat,
    generateRandomNumber,
    readFile,
    writeFile
}
