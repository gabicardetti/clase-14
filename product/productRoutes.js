const baseProduct = "/productos";
const express = require("express");


const ProductController = require("./productController.js");

const routes = express.Router();

routes.get(baseProduct + "/listar", ProductController.getAllProducts);

routes.get(baseProduct + "/listar/:id([0-9]+)", ProductController.getProductById);

routes.post(baseProduct + "/guardar/", ProductController.createNewProduct);

routes.put(baseProduct + "/actualizar/:id([0-9]+)", ProductController.updateProductById);

routes.delete(baseProduct + "/borrar/:id([0-9]+)", ProductController.deleteProductById);

module.exports = routes;