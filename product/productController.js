const ProductService = require("./productService.js");

const getAllProducts = async (req, res) => {
  /**
   * Devuelve array de productos
   */
  const products = ProductService.getAllProducts();
  if (products.length > 0) {
    res.send({ products });
  } else {
    res.status(400).send({ error: "No hay productos cargados" });
  }
};

const getProductById = async (req, res) => {
  /**
   * Devuelve producto por id
   */
  const productId = Number(req.params.id);
  const product = ProductService.getProductById(productId);

  if (product) {
    res.send({ product });
  } else {
    res.status(400).send({ error: "Producto no encontrado" });
  }
};

const createNewProduct = (req, res) => {
  /**
   * Devuelve el producto incorporado
   */

  const { title, price, thumbnail } = req.body;

  const product = ProductService.generateNewProduct(title, price, thumbnail);
  res.send(product);
  const io = req.app.locals.io;

  io.emit("new product", product);
};

const updateProductById = async (req, res) => {
  /**
   * Actualiza un producto
   */
  const productId = Number(req.params.id);

  const { title, price, thumbnail } = req.body;
  const product = ProductService.updateProductById(
    productId,
    title,
    price,
    thumbnail
  );

  if (product) {
    res.send({ product });
  } else {
    res.status(400).send({ error: "Producto no encontrado" });
  }
};

const deleteProductById = async (req, res) => {
  /**
   * Elimina un producto
   */
  const productId = Number(req.params.id);
  const product = ProductService.deleteProductById(productId);

  if (product) {
    res.send({ product });
  } else {
    res.status(400).send({ error: "Producto no encontrado" });
  }
};

module.exports = {
  getAllProducts,
  getProductById,
  createNewProduct,
  updateProductById,
  deleteProductById,
};
