const Util = require("../util/utils.js");
let products = [];

const getAllProducts = () => products;

const generateNewProduct = (title, price, thumbnail) => {
  // hago lenght + 1 para que los id empiezen desde 1 y no desde 0
  const product = generateProduct(products.length + 1);
  if (title) product.title = title;
  if (price) product.price = price;
  if (thumbnail) product.thumbnail = thumbnail;

  products.push(product);
  return product;
};

const getProductById = (id) => {
  const product = products.find((p) => p.id == id);
  return product;
};

const deleteProductById = (id) => {
  const product = getProductById(id);
  products = products.filter((p) => p.id != id);
  return product;
};

const updateProductById = (id, title, price, thumbnail) => {
  const index = products.findIndex((p) => p.id == id);
  if (index === -1) return;

  if (title) products[index].title = title;
  if (price) products[index].price = price;
  if (thumbnail) products[index].thumbnail = thumbnail;

  return products[index];
};

function generateProduct(id) {
  return {
    id,
    title: "Producto " + Util.generateRandomNumber(1, 10),
    price: Util.generateRandomNumberFloat(0.0, 9999.99),
    thumbnail: "Foto " + Util.generateRandomNumber(1, 10),
  };
}

module.exports = {
  getAllProducts,
  generateNewProduct,
  getProductById,
  deleteProductById,
  updateProductById,
};
