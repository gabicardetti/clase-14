const baseChat = "/chat";
const express = require("express");


const ChatController = require("./chatController.js");

const routes = express.Router();

routes.get(baseChat + "/", ChatController.getAllMessages);

module.exports = routes;