const File = require("../util/fileUtil.js");
const fileName = "chatHistory";

const saveNewMessage = (msg) => {
  const chatFile = new File(fileName);
  chatFile.save(msg);
};

const getAllMessagesFromFile = async () => {
  const chatFile = new File(fileName);
  const chatHistory = await chatFile.read();

  return JSON.parse(chatHistory);
};

module.exports = {
  saveNewMessage,
  getAllMessagesFromFile,
};
