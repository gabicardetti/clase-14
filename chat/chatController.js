const ChatService = require("./chatService.js");

const getAllMessages = async (req, res) => {
  const messages = await ChatService.getAllMessagesFromFile();

  res.send({ messages });
};

module.exports = {
  getAllMessages
};
